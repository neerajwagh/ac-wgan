import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('** Using device:', device)
print('** Device name: ', torch.cuda.get_device_name(0))

num_epochs = 100
#learning_rate = 0.01
learning_rate = 0.0001
num_classes = 10
batch_size = 128

class Discriminator(nn.Module):
    def __init__(self):

        super(Discriminator, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[32, 32]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[16, 16]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[16, 16]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[4, 4]),
            nn.LeakyReLU(inplace=True),

            nn.MaxPool2d(kernel_size=4, stride=4, padding=0)
        )

        self.fc1 = nn.Linear(196, 1)
        self.fc10 = nn.Linear(196, 10)

    def forward(self, x):

        x = self.conv(x)
        x = x.view(x.size(0), -1)

        out_fc1 = self.fc1(x)
        out_fc10 = self.fc10(x)

        return out_fc1, out_fc10

transform_train = transforms.Compose([
    transforms.RandomResizedCrop(32, scale=(0.7, 1.0), ratio=(1.0,1.0)),
    transforms.ColorJitter( 
            brightness=0.1*torch.randn(1),
            contrast=0.1*torch.randn(1),
            saturation=0.1*torch.randn(1),
            hue=0.1*torch.randn(1)),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

transform_test = transforms.Compose([
    transforms.CenterCrop(32),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

trainset = torchvision.datasets.CIFAR10(root='./', train=True, download=True, transform=transform_train)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=8)

testset = torchvision.datasets.CIFAR10(root='./', train=False, download=False, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=8)

# model =  Discriminator()
model = torch.load('cifar10.model')
model.to(device)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)

# for epoch in range(1, num_epochs+1):

#     if(epoch==50):
#         for param_group in optimizer.param_groups:
#             param_group['lr'] = learning_rate/10.0
#     if(epoch==75):
#         for param_group in optimizer.param_groups:
#             param_group['lr'] = learning_rate/100.0

#     total_correct = 0
#     total_samples = 0

#     for batch_idx, (X_train_batch, Y_train_batch) in enumerate(trainloader):

#         if(Y_train_batch.shape[0] < batch_size):
#             continue

#         X_train_batch = Variable(X_train_batch).to(device)
#         Y_train_batch = Variable(Y_train_batch).to(device)
#         _, outputs = model(X_train_batch)

#         loss = criterion(outputs, Y_train_batch)
#         optimizer.zero_grad()

#         loss.backward()

#         if(epoch>6):
#             for group in optimizer.param_groups:
#                 for p in group['params']:
#                     state = optimizer.state[p]
#                     if 'step' in state.keys():
#                         if(state['step']>=1024):
#                             state['step'] = 1000

#         optimizer.step()

#         _, predict_label = torch.max(outputs, 1)
#         total_samples += Y_train_batch.shape[0]
#         total_correct += predict_label.eq(Y_train_batch.long()).float().sum().item()
#     training_accuracy = total_correct / total_samples

#     print("---- Training Accuracy after Epoch #", epoch+1, " : ", training_accuracy)

# torch.save(model, 'cifar10.model')
# print("** Trained model saved to disk! **")

model.eval()
with torch.no_grad():
    correct_predictions = 0
    total = 0
    for images, labels in testloader:
        images = images.to(device)
        labels = labels.to(device)
        _, outputs = model(images)
        _, predicted = torch.max(outputs, 1)
        total += labels.size(0)
        correct_predictions += (predicted == labels).sum().item()

    test_acc = (correct_predictions / total) * 100 
    print("** Accuracy on Test Set: ", test_acc)

print ("** Done! **")
