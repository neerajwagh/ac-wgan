import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.optim as optim
from torch import autograd
from torch.autograd import Variable

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import numpy as np
import time

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('** Using device:', device)
print('** Device name: ', torch.cuda.get_device_name(0))

batch_size = 128

class Generator(nn.Module):
    def __init__(self):

        super(Generator, self).__init__()

        self.fc1 = nn.Sequential(
            nn.Linear(100, 196 * 4 * 4),
            nn.BatchNorm1d(196 * 4 * 4)
        )

        self.conv = nn.Sequential(

            nn.ConvTranspose2d(in_channels=196, out_channels=196, kernel_size=4, padding=1, stride=2),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.ConvTranspose2d(in_channels=196, out_channels=196, kernel_size=4, padding=1, stride=2),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.ConvTranspose2d(in_channels=196, out_channels=196, kernel_size=4, padding=1, stride=2),
            nn.BatchNorm2d(num_features=196),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=3, kernel_size=3, padding=1, stride=1),
            nn.Tanh()
        )

    def forward(self, x):
        x = self.fc1(x)
        x = x.view(-1, 196, 4, 4)
        x = self.conv(x)

        return(x)

class Discriminator(nn.Module):
    def __init__(self):

        super(Discriminator, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[32, 32]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[16, 16]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[16, 16]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=1),
            nn.LayerNorm(normalized_shape=[8, 8]),
            nn.LeakyReLU(inplace=True),

            nn.Conv2d(in_channels=196, out_channels=196, kernel_size=3, padding=1, stride=2),
            nn.LayerNorm(normalized_shape=[4, 4]),
            nn.LeakyReLU(inplace=True),

            nn.MaxPool2d(kernel_size=4, stride=4, padding=0)
        )

        self.fc1 = nn.Linear(196, 1)
        self.fc10 = nn.Linear(196, 10)

    def forward(self, x):

        x = self.conv(x)
        x = x.view(x.size(0), -1)

        out_fc1 = self.fc1(x)
        out_fc10 = self.fc10(x)

        return out_fc1, out_fc10

def plot(samples):
    fig = plt.figure(figsize=(10, 10))
    gs = gridspec.GridSpec(10, 10)
    gs.update(wspace=0.02, hspace=0.02)

    for i, sample in enumerate(samples):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        plt.imshow(sample)
    return fig


transform_test = transforms.Compose([
    transforms.CenterCrop(32),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

testset = torchvision.datasets.CIFAR10(root='./', train=False, download=False, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=8)
testloader = iter(testloader)

X_batch, Y_batch = testloader.next()
X_batch = Variable(X_batch,requires_grad=True).to(device)

X = X_batch.mean(dim=0)
X = X.repeat(10,1,1,1)

Y = torch.arange(10).type(torch.int64)
Y = Variable(Y).to(device)

disc_only_model = torch.load('cifar10.model')
disc_only_model.to(device).eval()

disc_gen_model = torch.load('discriminator.model')
disc_gen_model.to(device).eval()

for model in [disc_only_model, disc_gen_model]:

    lr = 0.1
    weight_decay = 0.001

    for i in range(200):
        _, output = model(X)

        loss = -output[torch.arange(10).type(torch.int64),torch.arange(10).type(torch.int64)]
        gradients = torch.autograd.grad(outputs=loss, inputs=X,
                                  grad_outputs=torch.ones(loss.size()).to(device),
                                  create_graph=True, retain_graph=False, only_inputs=True)[0]

        prediction = output.data.max(1)[1]
        accuracy = ( float( prediction.eq(Y.data).sum() ) /float(10.0))*100.0
        print(i,accuracy,-loss)

        X = X - lr*gradients.data - weight_decay*X.data*torch.abs(X.data)
        X[X>1.0] = 1.0
        X[X<-1.0] = -1.0

    samples = X.data.cpu().numpy()
    samples += 1.0
    samples /= 2.0
    samples = samples.transpose(0,2,3,1)

    fig = plot(samples)
    
    if (model == disc_only_model):
        plt.savefig('visualization/max_class_{title}.png'.format(title="Disc_Only"), bbox_inches='tight')
    else:
        plt.savefig('visualization/max_class_{title}.png'.format(title="Disc_Gen"), bbox_inches='tight')
        
    plt.close(fig)
    print("** Plots saved! **")

print("** Done! **")